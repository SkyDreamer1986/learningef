﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;

using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace DataModel
{
    public class Notifier : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public void notifyPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ActionCommand : ICommand
    {
        Action _action;

        public ActionCommand(Action action)
        {
            _action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            if (parameter is Action)
            {
                Action a = (Action)parameter;
                a();
            }
            else
            {
                _action();
            }
        }
    }

    /// <summary>
    /// Пол физического лица
    /// </summary>
    [Table("Gender")]
    public class Gender
    {
        public Guid id { get; set; }
        public string name { get; set; }
    }

    /// <summary>
    /// Тип адреса
    /// </summary>
    [Table("AddressType")]
    public class AddressType
    {
        public AddressType()
        {
            this.addresses = new HashSet<Address>();
        }

        public Guid id { get; set; }
        public string name { get; set; }
        public virtual ICollection<Address> addresses { get; set; }
    }

    /// <summary>
    /// Адрес
    /// </summary>
    [Table("Address")]
    public class Address
    {
        public Address()
        {
            id = Guid.NewGuid();
        }

        public Guid id { get; set; }
        public string postalCode { get; set; }
        public string country { get; set; }
        public string region { get; set; }
        public string district { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string house { get; set; }
        public string flat { get; set; }

        public Guid? addressTypeId { get; set; }
        public AddressType addressType { get; set; }

        public Guid? personId { get; set; }
        public Person person { get; set; }
    }

    /// <summary>
    /// Тип контакта
    /// </summary>
    [Table("ContactType")]
    public class ContactType 
    {
        public ContactType()
        {
            id = Guid.NewGuid();
            this.contacts = new HashSet<Contact>();
        }

        public Guid id { get; set; }
        public string data { get; set; }
        public virtual ICollection<Contact> contacts { get; set; }
    }

    /// <summary>
    /// Контакт
    /// </summary>
    [Table("Contact")]
    public class Contact
    {
        public Contact()
        {
            id = Guid.NewGuid();
        }

        public Guid id { get; set; }
        public string data { get; set; }
        public string description { get; set; }

        public Guid? contactTypeId { get; set; }
        public ContactType contactType { get; set; }

        public Guid? personId { get; set; }
        public Person person { get; set; }
    }

    /// <summary>
    /// Тип документа физического лица
    /// </summary>
    [Table("PersonDocumentType")]
    public class PersonDocumentType
    {
        public PersonDocumentType()
        {
            this.documents = new HashSet<PersonDocument>();
            this.id = Guid.NewGuid();
        }

        private Guid _id;
        public Guid id 
        {
            get 
            { 
                return _id; 
            }

            set 
            { 
                _id = value; 
                
            } 
        }

        private string _data;
        public string data {
            get { return _data; }
            set 
            { 
                _data = value; 
                //this.notifyPropertyChanged("data");
            }
        }

        public virtual ICollection<PersonDocument> documents { get; set; }
    }

    /// <summary>
    /// Документ физического лица
    /// </summary>
    [Table("PersonDocument")]
    public class PersonDocument : Notifier
    {
        public PersonDocument()
        {
            id = Guid.NewGuid();
        }

        public Guid id { get; set; }
        public string serial { get; set; }
        public string number { get; set; }
        public DateTime issueDate { get; set; }
        public DateTime expireDate { get; set; }
        public string issuer { get; set; }

        public Guid? documentTypeId { get; set; }
        public PersonDocumentType documentType { get; set; }

        public virtual Guid personId { get; set; }
        public Person person { get; set; }
    }

    /// <summary>
    /// Физическое лицо
    /// </summary>
    [Table("Person")]
    public class Person : Notifier
    {
        public Person()
        {
            id = Guid.NewGuid();
            this.addresses = new HashSet<Address>();
            this.contacts = new HashSet<Contact>();
            this.documents = new HashSet<PersonDocument>();
        }

        public Guid id { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public DateTime? dateOfBirth { get; set; }
        public Gender gender { get; set; }
        public Guid? genderId { get; set; }

        public virtual ICollection<Address> addresses { get; set; }
        public virtual ICollection<Contact> contacts { get; set; }
        public virtual ICollection<PersonDocument> documents { get; set; }
    }

    /// <summary>
    /// Учетная запись физического лица
    /// </summary>
    [Table("UserAccount")]
    public class UserAccount : Notifier
    {
        public UserAccount()
        {
            id = Guid.NewGuid();
        }

        public Guid id { get; set; }
        public string login { get; set; }
        public string passwordHash { get; set; }

        public Guid personId { get; set; }
        public Person person { get; set; }
    }

    /// <summary>
    /// Тип источника
    /// </summary>
    [Table("DestinationType")]
    public class DestinationType : Notifier
    {
        public DestinationType()
        {
            id = Guid.NewGuid();
            sources = new HashSet<Source>();
        }

        public Guid id { get; set; }
        public string data { get; set; }

        public ICollection<Source> sources;
    }

    /// <summary>
    /// Формат данных, в котором сохранено 
    /// </summary>
    [Table("DataFormat")]
    public class DataFormat : Notifier
    {
        public DataFormat()
        {
            id = Guid.NewGuid();
            sources = new HashSet<Source>();
        }

        public Guid id { get; set; }
        public string data { get; set; }

        public virtual ICollection<Source> sources { get; set; }
    }

    /// <summary>
    /// Пользовательские тэги, ассоциированные с источником информации
    /// </summary>
    [Table("SourceTag")]
    public class SourceTag : Notifier
    {
        public SourceTag()
        {
            id = Guid.NewGuid();
            sources = new HashSet<Source>();
        }

        public Guid id { get; set; }
        public string data { get; set; }

        public ICollection<Source> sources;
    }

    /// <summary>
    /// Источник информации (книга, журнал, рассказ человека и т.д.)
    /// В текущей реализации не предполагается того, что содержимое источника будет храниться целиком в БД
    /// :TODO Может, стоит добавить поле для его хранения все-таки? Это даст возможность, например, записывать беседы 
    /// на диктофон и т.д. и приложить ...
    /// </summary>
    [Table("Source")]
    public class Source : Notifier
    {
        public Source()
        {
            id = Guid.NewGuid();
            sourceTags = new HashSet<SourceTag>();
            //linkedSources = new HashSet<Source>();
        }

        public Guid id { get; set; }
        public string destination { get; set; } // Где находится источник

        public string description { get; set; } // Описание

        public Guid? userAccountId { get; set; }
        public UserAccount userAccount { get; set; }

        public bool saved { get; set; } // Сохранен ли объект

        public Guid? dataFormatId { get; set; }
        public DataFormat dataFormat { get; set; } // Формат данных, в котором сохранен источник (PDF, DJVU, AVI или что-то еще)
        public byte[] data { get; set; }

        public Guid? sourceTagId { get; set; }
        public ICollection<SourceTag> sourceTags;

        public ICollection<SourceLink> sourceLinks { get; set; }

        public ICollection<SourceLink> destinationLinks { get; set; }
    }

    [Table("SourceLink")]
    public class SourceLink : Notifier
    {
        public SourceLink()
        {
            id = Guid.NewGuid();
        }

        public Guid id { get; set; }

        public Guid userAccountId { get; set; }
        public UserAccount userAccount { get; set; }

        public DateTime dateAdd { get; set; }

        public string description { get; set; }

        public Guid sourceLinkId { get; set; }

        //[ForeignKey("sourceLinkId")]
        public Source sourceLink { get; set; }

        public Guid destinationLinkId { get; set; }

        //[ForeignKey("destinationLinkId")]
        public Source destinationLink { get; set; }
    }

    public class DataBaseInitializerDev : DropCreateDatabaseAlways<DBContext>
    {
        public DataBaseInitializerDev()
        {

        }
    }

    public class DataBaseInitializerWork : CreateDatabaseIfNotExists<DBContext>
    {
        public DataBaseInitializerWork()
        {

        }
    }

    public class SourceLinkLinkEntityConfiguration : EntityTypeConfiguration<SourceLink>
    {
        public SourceLinkLinkEntityConfiguration()
        {
            // Позволяем создать таблицу sourceLink путем указания того, что каскадное удаление выполняться не будет
            this.HasRequired<Source>(c => c.destinationLink)
                .WithMany(c => c.destinationLinks)
                .HasForeignKey(c => c.destinationLinkId)
                .WillCascadeOnDelete(false);
        }
    }

    public class DBContext : DbContext
    {
        public DBContext()
            : base("LearningEF")
        {
            
        }

        public DBContext(IDatabaseInitializer<DBContext> dbInitializer)
        {
            if (dbInitializer is DataBaseInitializerDev)
                prepareNewDataBase();
        }

        protected void prepareNewDataBase()
        {
            Database.SetInitializer<DBContext>(new DataBaseInitializerDev());

            System.Console.WriteLine("Запущено создание БД, пожалуйста, подождите ...");
            System.Console.WriteLine("Заполняем таблицу Gender");

            genders.Add(new Gender() { id = Guid.NewGuid(), name = "Мужской" });
            genders.Add(new Gender() { id = Guid.NewGuid(), name = "Женский" });

            this.SaveChanges();

            System.Console.WriteLine(genders.Count());

            System.Console.WriteLine("Заполняем таблицу PersonDocumentType");

            personDocumentTypes.Add(new PersonDocumentType { id = Guid.NewGuid(), data = "Паспорт РФ" });
            personDocumentTypes.Add(new PersonDocumentType { id = Guid.NewGuid(), data = "Паспорт иностранного гражданина" });
            personDocumentTypes.Add(new PersonDocumentType { id = Guid.NewGuid(), data = "Вид на жительство" });

            this.SaveChanges();

            System.Console.WriteLine("Добавляем ФЛ");

            persons.Add(new Person
            {
                id = Guid.NewGuid()
                , firstName = "Сергей"
                , middleName = "Александрович"
                , lastName = "Болтунов"
                , dateOfBirth = DateTime.Parse("31.08.1986")
                , gender = (genders.Where(g => g.name == "Мужской")).First<Gender>()
            });

            SaveChanges();

            System.Console.WriteLine("Добавляем учетную запись");

            userAccounts.Add(new UserAccount()
            {
                id = Guid.NewGuid()
                , login = "boltunov_s_a"
                , passwordHash = "admin".GetHashCode().ToString()
                , person = (from p in persons
                          where p.lastName == "Болтунов"
                          select p).First<Person>()
            });

            SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new SourceLinkLinkEntityConfiguration());
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Gender> genders { get; set; }
        public DbSet<AddressType> addressTypes { get; set; }
        public DbSet<Address> addresses { get; set; }
        public DbSet<PersonDocumentType> personDocumentTypes { get; set; }
        public DbSet<PersonDocument> personDocuments { get; set; }
        public DbSet<Person> persons { get; set; }
        public DbSet<ContactType> contactTypes { get; set; }
        public DbSet<Contact> contacts { get; set; }
        public DbSet<UserAccount> userAccounts { get; set; }
        public DbSet<Source> sources { get; set; }
        public DbSet<SourceLink> sourceLinks { get; set; }
    }

    class DataModel
    {
        static void Main(string[] args)
        {
            using (DBContext db = new DBContext(new DataBaseInitializerDev()))
            {
                db.SaveChanges();
            }
        }
    }
}
