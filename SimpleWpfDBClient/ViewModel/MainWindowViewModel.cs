﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core;
using DataModel;

namespace SimpleWpfDBClient
{
    public class MainWindowViewModel : Notifier
    {
        public DBContext dbContext { get; set; }

        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    for (int i = 0; i < e.NewItems.Count; i++)
                    {
                       // (e.NewItems[i] as DataModel.PersonDocumentType).id = Guid.NewGuid();
                    }    
                    break;
                case NotifyCollectionChangedAction.Remove: break;
            }
        }
          
        public MainWindowViewModel()
        {
            dbContext = new DBContext();
            dbContext.personDocumentTypes.Load();
            dbContext.personDocumentTypes.Local.CollectionChanged += OnCollectionChanged;
        }
        
        private ActionCommand _save;
        public ICommand save
        {
            get 
                {
                    return _save = _save ?? new ActionCommand(() => 
                    { 
                        dbContext.SaveChanges(); 
                    }
                    );
                }
        }

        private ActionCommand _restore;
        public ActionCommand restore
        {
            get
            {
                return _restore = _restore ?? new ActionCommand(() => { dbContext.personDocumentTypes.Load(); });
            }
        }

        private ActionCommand _addRow;
        public ActionCommand addRow
        {
            get 
            {
                //
                return _addRow ?? (_addRow = new ActionCommand(() => 
                    {
                        dbContext.personDocumentTypes.Add(
                            new PersonDocumentType() { id = Guid.NewGuid(), data = "Новый документ"});
                        
                    }
                    )); 
            }
        }

        private ActionCommand _showClientDocuments;
        public ActionCommand showClientDocuments
        {
            get { return _showClientDocuments = _showClientDocuments ?? new ActionCommand(() => { }); }
        }
    }
}
