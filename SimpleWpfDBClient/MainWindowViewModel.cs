﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core;
using DataModel;

namespace SimpleWpfDBClient
{
    public class MainWindowViewModel : Notifier
    {
        public DBContext dbContext { get; set; }

        /*
        private DataModel.PersonDocumentType _docTypes;

        public DataModel.PersonDocumentType docTypes {
            get 
            {
                return _docTypes;
            }

            set
            {
                _docTypes = value; notifyPropertyChanged("docTypes");
                
            }
        }
        */
          
        public MainWindowViewModel()
        {
            dbContext = new DBContext();

            dbContext.personDocumentTypes.Load();
            // docTypes = dbContext.personDocumentTypes.Local;
            
            /*
            foreach (var d in dbContext.personDocumentTypes)
                MessageBox.Show(d.data);
            */
            
            /*
            op1 = 1;
            op2 = 2;
            result = op1 + op2;
             * */
        }

        private double _op1;
        public double op1
        {
            set { _op1 = value; notifyPropertyChanged("op1"); }
            get { return _op1; }
        }

        private double _op2;
        public double op2
        {
            set { _op2 = value; notifyPropertyChanged("op2"); }
            get { return _op2; }
        }

        private double _result;
        public double result
        {
            set { _result = value; notifyPropertyChanged("result"); }
            get { return _result; }
        }
        
        private ActionCommand _calc;
        public ICommand calc
        {
            get 
                {
                    return _calc = _calc ?? new ActionCommand(() => { result = op1 + op2; dbContext.SaveChanges(); MessageBox.Show("Changed!"); });
                }
        }

        private ActionCommand _addRow;
        public ActionCommand addRow
        {
            get 
            {
                //
                return _addRow ?? (_addRow = new ActionCommand(() => 
                    {
                        dbContext.personDocumentTypes.Add(
                            new PersonDocumentType() { id = Guid.NewGuid(), data = "Новый документ"}); 
                    }
                    )); 
            }
        }
    }
    
    public class ActionCommand : ICommand
    {
        Action _action;

        public ActionCommand(Action action)
        {
            _action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            if (parameter is Action)
            {
                Action a = (Action)parameter;
                a();
            }
            else
            {
                _action();
            }
        }
    }
}
